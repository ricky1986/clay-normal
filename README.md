# clay-normal

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### elment plus 按需引入示例
```
// 编辑/src/plugins/element.js

// 在这里import 要使用的组件
import { ElButton, ElTooltip, ElPopover, ElDialog } from 'element-plus'
import lang from 'element-plus/lib/locale/lang/zh-cn'
import locale from 'element-plus/lib/locale'

export default (app) => {
  locale.use(lang)

  // 在这里use要使用的组件
  app.use(ElButton).use(ElTooltip).use(ElPopover).use(ElDialog)
}

```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
